public class I{
    private long t;

    public I(long t){
        this.t=t;
    }

    public void f(){

    }
    public void i( J j){

    }

    public static void main(String[] args){
            I i2= new I(10);
            I i1= new I(15);
          //  K k1= new K(20);
        System.out.println(i1.t);
        System.out.println(i2.t);
        //System.out.println(k1.k);

    }

}
interface U {
    public void u();
}

class K extends I{
    public K(long k){
        super(k);
    }

}

class J extends I {

    public J(long j){
        super(j);
    }

}

class N {

}

class L {
    public void metA(){

    }

}

class S {
    public void metB(){

    }
}
